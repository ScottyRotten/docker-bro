# Setup for HELK-Bro Host Docker Networks
# Reference: http://stevemcgrath.io/2017/07/docker-containers--network-sniffing/
# Requirements: Ubuntu 16.04 // 2 Network Interfaces, 1 Management, 1 Sniffing // Docker

echo "[*] Pulling Helk-Bro Dockerfile"
docker pull hellor00t/helk-bro-base

echo "[*] Downloading Traffic Control Mirror Script"
wget -O /usr/bin/tc-mirror https://raw.githubusercontent.com/SteveMcGrath/mirror_tools/master/tc-mirror.sh
chmod 755 /usr/bin/tc-mirror

echo "[*] Interfaces available:"
ls /sys/class/net

echo "[*] Enter name of sniffing interface"

read interface

echo "[*] Adding Docker span to interfaces"

cat <<EOT >> /etc/network/interfaces
# Docker Bridge Span Interface
auto span0
iface span0 inet manual
        bridge_stp off
        bridge_fd 0
        bridge_maxwait 0
        bridge_ageing 0
        post-up /usr/bin/tc-mirror build $interface span0
        pre-down /usr/sbin/tc-mirror teardown $interface
EOT

echo "[*] Restarting networking service"
systemctl restart networking

echo "[*] Creating docker network"
docker network create -d bridge --internal -o com.docker.network.bridge.name=span0 span

echo "[*] Starting helk-bro container"
docker run -it -d --name=helk-bro hellor00t/helk-bro-base /bin/bash

echo "[*] Connect helk-bro docker container to Docker span interface"
docker network connect span helk-bro

echo "[*] Starting helk-bro service"
docker exec -it helk-bro /usr/local/bro/bin/broctl deploy

