# HELK script: HELK Bro Dockerfile
# HELK build Stage: Alpha
# Author: Scott (@hellor00t)
# License: GPL-3.0

# References: 
# http://stevemcgrath.io/2017/07/docker-containers--network-sniffing/

FROM hellor00t/helk-bro-base
LABEL maintainer="Scott - hellor00t"
LABEL description="Dockerfile base for the HELK Bro Container."

CMD ["/usr/local/bro/bin/broctl deploy"]