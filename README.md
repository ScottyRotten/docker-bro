# Setup Instructions

## Installation

1. Execute installation script on host `./setup.sh`

`docker build -t helk-bro .`
host - Location where Docker is installed
container - Bro container, can be found by executing `docker ps`


1. Execute installation script on host `./setup.sh`
2. Connect Docker Span Network to HELK-bro Container `docker network connect span helk-bro`
3. Edit bro config for interface `vim /usr/local/bro/etc/node.cfg`
2. Edit bro config file for protected network `vim /usr/local/bro/etc/networks.cfg`
3. Start bro `/usr/local/bro/bin/broctl deploy`
4. Verify bro is running `/usr/local/bro/bin/broctl status`


Create Docker Network
`docker network create -d bridge --internal -o com.docker.network.bridge.name=span0 bro-span`

Create Container From Image

`docker run -it -d --name=helk-bro hellor00t/helk-bro-base /bin/bash`

Connect Container to Docker Network

`docker network connect bro-span helk-bro`

Start Bro Service

`/usr/local/bro/bin/broctl deploy` && `/usr/local/bro/bin/broctl start`

## OR - Easy mode


`docker pull blacktop/bro`

`docker run -d --cap-add=NET_RAW --net=host -v `pwd`:/pcap:rw blacktop/bro -i ens38`

